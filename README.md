# Weather Report App

This is a weather report application that uses openweathermap api to displays a weather report for five cities in South Africa.

## Dependencies 
- java 8
- google gson 2.8.6
- unirest-java 30.10.00
- log4j 1.2.17 
- junit 4.13 

## Build Instructions
### Using Maven:
```
mvn clean install
<or>
mvn clean package

