package net.iqbusiness.services;

import net.iqbusiness.interfaces.WeatherReportViewer;
import net.iqbusiness.models.Weather;
import org.apache.log4j.Logger;

public class WeatherReporConsoletViewer implements WeatherReportViewer {

    private static final Logger LOGGER = Logger.getLogger(WeatherReporConsoletViewer.class);

    @Override
    public void displayWeatherReport(Weather weather) {
        try {
            String result = weather.toString();
            LOGGER.info(result);
        }
        catch (Exception e){
            LOGGER.error(e.getMessage());
        }
    }
}
