package net.iqbusiness.services;

import kong.unirest.Unirest;
import kong.unirest.json.JSONObject;
import net.iqbusiness.exception.InvalidCityException;
import net.iqbusiness.exception.InvalidKeyException;
import net.iqbusiness.interfaces.WeatherReport;
import net.iqbusiness.models.Weather;
import org.apache.log4j.Logger;

public class OpenWeatherMapAPIService implements WeatherReport {

    private static final Logger LOGGER = Logger.getLogger(OpenWeatherMapAPIService.class);

    @Override
    public Weather getWeatherReport(String city,String apiKey) throws Exception {
            String urlString = String.format("https://api.openweathermap.org/data/2.5/weather?q=%s&appid=%s&units=metric",city,apiKey);
            JSONObject response =
                    Unirest.get(urlString)
                            .asJson()
                            .getBody()
                            .getObject();

        int code = Unirest.get(urlString).asJson().getStatus();

        if(code == 401) {
            throw new InvalidKeyException("Invalid Key");
        }
        else if(code == 404){
            throw new InvalidCityException("Invalid City");
        }

        String temp = response.getJSONObject("main").getString("temp");
        String humidity = response.getJSONObject("main").getString("humidity");
        String windSpeed = response.getJSONObject("wind").getString("speed");
        String cloudiness = response.getJSONObject("clouds").getString("all");

        Weather weather = new Weather(temp,humidity,windSpeed,cloudiness);
        return weather;
    }
}
