package net.iqbusiness.services;

import net.iqbusiness.interfaces.WeatherReportViewer;
import net.iqbusiness.models.Weather;
import org.apache.log4j.Logger;

import javax.swing.*;

public class WeatherReporGuiViewer implements WeatherReportViewer {

    private static final Logger LOGGER = Logger.getLogger(WeatherReporGuiViewer.class);

    @Override
    public void displayWeatherReport(Weather weather) {
        try {
            JFrame f = new JFrame("Weather Report");
            JTextArea jt = new JTextArea(10, 20);

            String result = weather.toString();

            JPanel p = new JPanel();
            p.add(jt);
            jt.append(result);
            f.setVisible(true);

            f.add(p);
            f.setSize(500, 250);
        }
        catch (Exception e){
            LOGGER.error(e.getMessage());
        }
    }
}
