package net.iqbusiness.exception;

public class InvalidKeyException extends Exception {

   public InvalidKeyException(String s){
        super(s);
    }
}
