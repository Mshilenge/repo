package net.iqbusiness.exception;

public class InvalidCityException extends Exception{
    public InvalidCityException(String s){
        super(s);
    }
}
