package net.iqbusiness.models;

import java.util.Objects;

public class Weather {

    private String temp;
    private String humidity;
    private String windSpeed;
    private String cloudiness;

    public Weather(String temp, String humidity, String windSpeed, String cloudiness) {
        this.temp = temp;
        this.humidity = humidity;
        this.windSpeed = windSpeed;
        this.cloudiness = cloudiness;
    }

    public String getTemp() {
        return temp;
    }

    public void setTemp(String temp) {
        this.temp = temp;
    }

    public String getHumidity() {
        return humidity;
    }

    public void setHumidity(String humidity) {
        this.humidity = humidity;
    }

    public String getWindSpeed() {
        return windSpeed;
    }

    public void setWindSpeed(String windSpeed) {
        this.windSpeed = windSpeed;
    }

    public String getCloudiness() {
        return cloudiness;
    }

    public void setCloudiness(String cloudiness) {
        this.cloudiness = cloudiness;
    }

    @Override
    public String toString() {
        return "Current Temparature: " + temp +"\n"+
                "Current Humidity: " + humidity +"\n"+
                "Wind Speeds: " + windSpeed +"\n"+
                "Cloudiness: "  + cloudiness;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Weather weather = (Weather) o;
        return Objects.equals(temp, weather.temp) &&
                Objects.equals(humidity, weather.humidity) &&
                Objects.equals(windSpeed, weather.windSpeed) &&
                Objects.equals(cloudiness, weather.cloudiness);
    }

    @Override
    public int hashCode() {
        return Objects.hash(temp, humidity, windSpeed, cloudiness);
    }
}
