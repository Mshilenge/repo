package net.iqbusiness.interfaces;

import net.iqbusiness.models.Weather;

public interface WeatherReportViewer {
    void displayWeatherReport(Weather weather);
}
