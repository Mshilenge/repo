package net.iqbusiness.interfaces;

import net.iqbusiness.models.Weather;

public interface WeatherReport {
    Weather getWeatherReport(String city,String apiKey) throws Exception;
}
