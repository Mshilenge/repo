package net.iqbusiness;

import kong.unirest.json.JSONException;
import net.iqbusiness.exception.InvalidCityException;
import net.iqbusiness.exception.InvalidKeyException;
import net.iqbusiness.models.Weather;
import net.iqbusiness.services.OpenWeatherMapAPIService;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class OpenWeatherReportTest {

    @Test
    public void weatherPositiveResponseTest() throws Exception {
        String city = "Pretoria";
        String apiKey = "7be2384a89072521d9eef27cf4683047";
        OpenWeatherMapAPIService weatherReportImp = new OpenWeatherMapAPIService();
        Weather weather = weatherReportImp.getWeatherReport(city,apiKey);

        assertNotNull("Weather is null", weather);
        assertNotNull("Temperature is null", weather.getTemp());
        assertNotNull("Cloudiness is null", weather.getCloudiness());
        assertNotNull("Humidity is null", weather.getHumidity());
        assertNotNull("Wind Speed is null", weather.getWindSpeed());
    }

    @Test(expected = JSONException.class)
    public void emptyCityStringTest() throws Exception{
        String city = "";
        String apiKey = "7be2384a89072521d9eef27cf4683047";
        OpenWeatherMapAPIService weatherReportImp = new OpenWeatherMapAPIService();
        Weather weather = weatherReportImp.getWeatherReport(city,apiKey);

        assertNull("Invalid city",weather);
    }

    @Test(expected = InvalidCityException.class)
    public void nullCityStringTest() throws Exception{
        String city = null;
        String apiKey = "7be2384a89072521d9eef27cf4683047";
        OpenWeatherMapAPIService weatherReportImp = new OpenWeatherMapAPIService();
        Weather weather = weatherReportImp.getWeatherReport(city,apiKey);
    }

    @Test(expected = InvalidCityException.class)
    public void garbageCityStringTest() throws Exception {
        String city = "123213asdsdSDSAD";
        String apiKey = "7be2384a89072521d9eef27cf4683047";
        OpenWeatherMapAPIService weatherReportImp = new OpenWeatherMapAPIService();
        Weather weather = weatherReportImp.getWeatherReport(city,apiKey);
    }

    @Test(expected = InvalidCityException.class)
    public void numbersCityStringTest() throws Exception {
        String city = "123213";
        String apiKey = "7be2384a89072521d9eef27cf4683047";
        OpenWeatherMapAPIService weatherReportImp = new OpenWeatherMapAPIService();
        Weather weather = weatherReportImp.getWeatherReport(city,apiKey);
    }

    @Test(expected = InvalidKeyException.class)
    public void invalidAPIKeyTest() throws Exception{
        String city = "Pretoria";
        String apiKey = "7be2384a89072521d9eef27cf46830472222";
        OpenWeatherMapAPIService weatherReportImp = new OpenWeatherMapAPIService();
        Weather weather = weatherReportImp.getWeatherReport(city,apiKey);
    }

    @Test(expected = InvalidKeyException.class)
    public void invalidCityKeyTest() throws Exception{
        String city = "Pretoria";
        String apiKey = "7be2384a89072521d9eef27cf46830472222";
        OpenWeatherMapAPIService weatherReportImp = new OpenWeatherMapAPIService();
        Weather weather = weatherReportImp.getWeatherReport(city,apiKey);

    }


}
